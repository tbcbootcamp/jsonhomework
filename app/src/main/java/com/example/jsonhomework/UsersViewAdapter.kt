package com.example.jsonhomework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.users_recyclerview_layout.view.*

class UsersViewAdapter(
    private val users: MutableList<UserModel.Data>
) : RecyclerView.Adapter<UsersViewAdapter.UsersViewHolder>() {


    inner class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = users[adapterPosition]
            itemView.firstNameTextView.text = model.firstName
            itemView.lastNameTextView.text = model.lastName
            itemView.emailTextView.text = model.email
            Glide.with(itemView.context).load(model.avatar).into(itemView.imageView)
        }
    }

<<<<<<< HEAD
=======

>>>>>>> b942effb3ad540bafb4e698bb1cdc2de20d24922
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.users_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = users.size

<<<<<<< HEAD
    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
=======
    override fun onBindViewHolder(holder:UsersViewHolder, position: Int) {
>>>>>>> b942effb3ad540bafb4e698bb1cdc2de20d24922
        holder.onBind()
    }
}